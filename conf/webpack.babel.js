import webpack from 'webpack'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
export default function (env) {
    const extractSCSS=new ExtractTextPlugin({
        filename:"[name].[contenthash].css",
        disable:false
    });
    return {
        devtool: env === 'dev' ? 'source-map' : 'hidden-source-map',
        entry: {
            'index': './src/main.js',
        },
        output: {
            filename: '[name].[chunkhash].js',
            path: './dist'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                babelrc: false,
                                presets: [['es2015', {module: false}]],
                            }
                        }
                    ],
                    exclude: /node_modules/
                },
                {
                    test:/\.scss$/,
                    loader: extractSCSS.extract({
                        use: ["css-loader","sass-loader"],
                        // use style-loader in development
                        fallback: "style-loader"
                    })

                }
            ]
        },
        plugins: [
            // new webpack.optimize.CommonsChunkPlugin ({
            //     name: ['foundationCss', 'foundation', 'jquery','base64'],
            //     filename: 'vendor/[name].[chunkhash:7].js'
            // }),
            // new webpack.optimize.UglifyJsPlugin ({
            //     sourceMap: env === 'dev'
            // }),
            extractSCSS,
            new HtmlWebpackPlugin ({
                template: 'index.html',
                filename: './index.html'
            })
        ],

        resolve: {

        }
    }
}