import gulp from 'gulp'
import webpack from 'webpack'
import webpackCon from './conf/webpack.babel'
import del from 'del'

gulp.task('default',function () {
    
});
gulp.task('clean',function (cb) {
      return del(['./dist/**'],cb)
});
gulp.task('copy',['clean'],function () {
    return gulp.src('src/fontcustom/*')
        .pipe(gulp.dest('./dist/fontcustom/'))
});
gulp.task('webpack',['copy'],function () {
     webpack(webpackCon('dev'),function () {

     })
});