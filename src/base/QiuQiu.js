export default class QiuQiu {
    constructor(App) {
        this.AppDOM = App.AppDOM
    }

    attachElement(element) {
        this.rootElement = typeof element == 'string' ? document.querySelector(element) : element;
        this.rootElement && this.rootElement.appendChild(this.AppDOM)
    }
}
