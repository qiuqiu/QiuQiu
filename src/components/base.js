import Tools from '../base/tools'
import '../scss/base.scss'
export default class Base{
    constructor(idString,domString){
        let DOMStr=domString||'<div></div>';
        this.AppDOM=Tools.createElementByString(DOMStr);
        this.AppDOM &&this.AppDOM.setAttribute('id',idString)
    }
    appendDom(childDom){
        this.AppDOM.appendChild(childDom.AppDOM)
    }
    addClass(cString){
        this.AppDOM.classList.add(cString)
    }
    eventsBind(eventSort){

    }
}