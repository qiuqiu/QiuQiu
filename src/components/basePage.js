import Base from './base'
import Tools from '../base/tools'
import '../scss/basePage.scss'
export default class BasePage extends Base {
    constructor(params) {
        let domString = '<div class="app-base-page"><div class="app-base-page-title"><span class="app-base-page-title-text"></span></div><div class="app-base-page-content"></div><div class="app-base-page-bottom-nav"></div></div>';
        super(params['id'], domString);
    }

    set bottomNavItems(value) {
        try {
            this._bottomNavItems = value;
            let bottomNavDom = this.AppDOM.querySelector('.app-base-page-bottom-nav');
            Tools.removeAllChild(bottomNavDom);
            if (this._bottomNavItems.length == 0) {
                bottomNavDom.style.display = 'none'
            }
            else {
                for (let item of this._bottomNavItems) {
                    let cellStr = `<div class="app-base-page-bottom-nav-item"><i class="app-base-page-bottom-nav-icon ${item['iconStr']}"></i><br><span>${item['text']}</span></div>`;
                    let cellDom = Tools.createElementByString(cellStr);
                    bottomNavDom.appendChild(cellDom)
                }
                this.eventsBind('bottom-nav-touch')
            }

        }
        catch (e) {
            throw Error("bottom nav value error!")
        }

    }

    set basePageTitle(value) {
        this._basePageTitle = value;
        if(typeof value === 'string'){
            this.AppDOM.querySelector('.app-base-page-title-text').innerHTML = this._basePageTitle
        }
        else if(value instanceof HTMLElement){
            this.AppDOM.querySelector('.app-base-page-title-text').appendChild(this._basePageTitle)
        }

    }

    eventsBind(eventSort) {
        switch (eventSort) {
            case 'bottom-nav-touch':
                Array.from(this.AppDOM.querySelectorAll('.app-base-page-bottom-nav-item')).map(function (item) {
                    item.addEventListener('click', function (event) {
                        if (!event.currentTarget.classList.contains('selected')) {
                            let selectedNav=event.currentTarget.parentElement.querySelector('.selected')
                            if(selectedNav){
                                selectedNav.classList.remove('selected');
                            }
                            event.currentTarget.classList.add('selected')
                        }
                    })
                });

        }
    }
}