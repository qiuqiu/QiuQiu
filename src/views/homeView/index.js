import BasePage from '../../components/basePage'
import Base from '../../components/base'
import './style.scss'
export default class Homeview extends Base {
    constructor(id) {
        super(id);
        this.addClass('home-page');
        let bPage = new BasePage({id:'base-view'});
        bPage.bottomNavItems = [{iconStr: "icon-me", text: "我"},
            {iconStr:'icon-message',text:'消息'}];
        bPage.basePageTitle="首页";
        this.appendDom(bPage)
    }

}
